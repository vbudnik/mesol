$('#addDynamicExtraFieldButton').click(function(event) {
    addDynamicExtraField();
    return false;
});
function addDynamicExtraField() {
    var div = $('<div/>', {
        'class' : 'DynamicExtraField'
    }).appendTo($('#DynamicExtraFieldsContainer'));
    var br = $('<br/>').appendTo(div);
    var about = $('<label/>').html("Описание ").appendTo(div);
    var br = $('<br/>').appendTo(div);
    var textareaAbout = $('<textarea/>', {
        name : 'DynamicExtraFieldAbout[]',
        cols : '50',
        rows : '3'
    }).appendTo(div);

    var br = $('<br/>').appendTo(div);

    var label = $('<label/>').html("Доп. поле ").appendTo(div);
    var br = $('<br/>').appendTo(div);
    var textarea = $('<textarea/>', {
        name : 'DynamicExtraField[]',
        cols : '50',
        rows : '3'
    }).appendTo(div);
    var br = $('<br/>').appendTo(div);
    var input = $('<input/>', {
        value : 'Удаление',
        type : 'button',
        'class' : 'DeleteDynamicExtraField'
    }).appendTo(div);
    input.click(function() {
        $(this).parent().remove();
    });
}
//Для удаления первого поля
$('.DeleteDynamicExtraField').click(function(event) {
    $(this).parent().remove();
    return false;
});