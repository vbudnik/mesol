<?php
/**
 * Created by PhpStorm.
 * User: vbudnik
 * Date: 13.02.2019
 * Time: 20:33
 */

namespace app\models;


use yii\base\Model;

class Admin extends Model
{
    public $nameproject;
    public $site;
    public $name;
    public $email;
    public $password;

    public function rules()
    {
        return [
            [['nameproject', 'site', 'name', 'email', 'password'], 'required'],
            [['email'], 'email'],
            ['password', 'string', 'min'=>2, 'max'=>10]
        ];
    }

    public function admin() {
        $project = new Project();

        $project->name = $this->name;
        $project->nameproject = $this->nameproject;
        $project->site = $this->site;
        $project->email = $this->email;
        $project->setPassword($this->password);
    }
}