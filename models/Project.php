<?php
/**
 * Created by PhpStorm.
 * User: vbudnik
 * Date: 13.02.2019
 * Time: 20:40
 */

namespace app\models;


use yii\db\ActiveRecord;

class Project extends ActiveRecord
{
    public function setPassword($password) {
        $this->password = sha1($password);
    }
}