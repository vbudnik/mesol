<?php
/**
 * Created by PhpStorm.
 * User: remote_user
 * Date: 13.02.2019
 * Time: 16:22
 */

namespace app\models;


use yii\base\Model;

class Login extends Model
{
    public $email;
    public $password;

    public function rules()
    {
        return [
            [['email', 'password'], 'required'],
            ['email', 'email'],
            ['password', 'validatePassword']
        ];
    }

    public function validatePassword($attribute, $params) {
        if( !$this->hasErrors()) { // если нет ошибок в валидации
            $user = $this->getUser();
            if (!$user || !$user->validatePassword($this->password)) {
                $this->addError($attribute,
                    'Пароль не верный, или пользователь веден неверно');
            }
        }
    }

    public function getUser() {
        return User::findOne(['email'=>$this->email]);
    }
}