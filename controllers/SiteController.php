<?php

namespace app\controllers;

use app\models\Abstraction;
use app\models\Admin;
use app\models\Login;
use app\models\Signup;
use Yii;
use yii\web\Controller;
use app\models\LoginForm;
use app\models\ContactForm;

class SiteController extends Controller
{
    public function actionIndex() {
        return $this->render('index');
    }

    public function actionAdmin() {
        $project = new Admin();

        if(isset($_POST['Admin'])) {
            return $this->goHome();
        }
        return $this->render('admin', ['project'=>$project]);
    }

    public function actionCreate() {
        return $this->render('create');
    }

    public function actionManual() {
        return $this->render('manual');
    }

    public function actionLanguage() {
        return $this->render('language');
    }

    public function actionAbstraction() {
        $abstraction = new Abstraction();

        Yii::$app->request->post();


//        if (isset($_POST['Abstraction'])) {

//            echo "LOL";
        if(Yii::$app->request->post() != null) {
            echo "<pre>";

                foreach ($_POST as $key => $val) {
                    var_dump($key);
                    echo "value \n";
                    var_dump($val);
                }
            echo "</pre>";
            exit(1);

        }

        return $this->render('abstraction', ['abstraction' => $abstraction]);
    }

    public function actionHelping() {
        return $this->render('helping');
    }

    public function actionLists() {
        return $this->render('lists');
    }

    public function actionIvent() {
        return $this->render('ivent');
    }

    public function actionLogin() {
        return $this->render('login');
    }

    public function actionOrganization() {
        return $this->render('organization');
    }

}
