<h1>Абстракт (язык)</h1>

<?php
    use \yii\widgets\ActiveForm;
    use \yii\helpers\Html;
?>

<?php $form = ActiveForm::begin() ?>
<!--<form action="" method="post" name="Form">-->
    <div id="DynamicExtraFieldsContainer">
        <div class="addDynamicField">
            <input type="button" id="addDynamicExtraFieldButton" value="Добавить динамическое поле">
        </div>
    </div>
    <br>
    <button type="submit" class="btn btn-primary">Создать</button>
<?php $form = ActiveForm::end() ?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>


<script>

    var i = 0;

    $('#addDynamicExtraFieldButton').click(function(event) {
        addDynamicExtraField(i);
        i = i + 1;
        return false;
    });


    function addDynamicExtraField(i) {
        var div = $('<div/>', {
            'class': 'DynamicExtraField'
        }).appendTo($('#DynamicExtraFieldsContainer'));
        selectTypeForm(div, i);
    }

    function selectTypeForm(div, i) {
        var j = i;
        var br = $('<br/>').appendTo(div);
        var label = $('<label/>').html("Select one options").appendTo(div);
        var br = $('<br/>').appendTo(div);
        var id = '"DynamicExtraFieldSelect-'+j+'"';
        var select = $('<select id= ' +id+' >\n' +
            '        <option value="1" selected="selected">None</option>\n' +
            '        <option value="2">Текст</option>\n' +
            '        <option value="3">Один из списка</option>\n' +
            '        <option value="4">Несколько из списка</option>\n' +
            '    </select>').appendTo(div);
        $('#DynamicExtraFieldSelect-'+j).change(function (event) {
            var e = document.getElementById("DynamicExtraFieldSelect-" + j);
            var typeForm = e.options[e.selectedIndex].value;
            $('#DynamicExtraFieldSelect-'+j).remove();
            $(label).remove();
            if (typeForm == '2') {
                addTextArea(div, j);
                return true;
            } else if (typeForm == '3') {

                var about = $('<label/>').html("Вопрос").appendTo(div);
                var br = $('<br/>').appendTo(div);
                var textareaAbout = $('<textarea/>', {
                    name : "DynamicExtraFieldOneListQuestion-"+ j +"[]",
                    cols : '50',
                    rows : '1'
                }).appendTo(div);
                var br = $('<br/>').appendTo(div);
                var br = $('<br/>').appendTo(div);
                var about = $('<label/>').html("Описание ").appendTo(div);
                var br = $('<br/>').appendTo(div);
                var textareaAbout = $('<textarea/>', {
                    name : "DynamicExtraFieldOneListAnswer-"+ j +"[]",
                    cols : '50',
                    rows : '1'
                }).appendTo(div);
                var br = $('<br/>').appendTo(div);
                addNewRowToList(div, j);
            } else if (typeForm == '4') {
                var about = $('<label/>').html("Вопрос").appendTo(div);
                var br = $('<br/>').appendTo(div);
                var textareaAbout = $('<textarea/>', {
                    name : "DynamicExtraFieldSomeQuestion-"+ j +"[]",
                    cols : '50',
                    rows : '1'
                }).appendTo(div);
                var br = $('<br/>').appendTo(div);
                var br = $('<br/>').appendTo(div);
                var about = $('<label/>').html("Описание ").appendTo(div);
                var br = $('<br/>').appendTo(div);
                var textareaAbout = $('<textarea/>', {
                    name : "DynamicExtraFieldSomeAnswer-"+ j +"[]",
                    cols : '50',
                    rows : '1'
                }).appendTo(div);
                var br = $('<br/>').appendTo(div);
                addNewRowToSome(div, j);
            }
        });

        return true;
    }

    function addTextArea(div, j){
        var label = $('<label/>').html("Input text").appendTo(div);
        var br = $('<br/>').appendTo(div);
        var label = $('<label/>').html("Описание текста").appendTo(div);
        var br = $('<br/>').appendTo(div);
        var textarea = $('<textarea/>', {
            name: "DynamicExtraFieldText-" + j + "[]",
            cols: '50',
            rows: '1'
        }).appendTo(div);
        var br = $('<br/>').appendTo(div);
        var input = $('<input/>', {
            value: 'Удаление',
            type: 'button',
            'class': 'DeleteDynamicExtraField'
        }).appendTo(div);
        input.click(function () {
            $(this).parent().remove();
        });
        return true;
    }

    function addNewRowToSome(div, j) {
        var br = $('<br/>').appendTo(div);
        var about = $('<label/>').html("Описание ").appendTo(div);
        var br = $('<br/>').appendTo(div);
        var textareaAbout = $('<textarea/>', {
            name : "DynamicExtraFieldSomeAnswer-"+ j +"[]",
            cols : '50',
            rows : '1'
        }).appendTo(div);

        var br = $('<br/>').appendTo(div);
        var add = $('<input/>', {
            value : 'Добавить',
            type : 'button',
            'class' : 'AddDynamicExtraField'
        }).appendTo(div);
        add.click(function() {
            add.remove();
            addNewRowToSome(div, j);
        });
    }

    function addNewRowToList(div, j) {
        var br = $('<br/>').appendTo(div);
        var about = $('<label/>').html("Описание ").appendTo(div);
        var br = $('<br/>').appendTo(div);
        var textareaAbout = $('<textarea/>', {
            name : "DynamicExtraFieldOneListAnswer-"+ j +"[]",
            cols : '50',
            rows : '1'
        }).appendTo(div);

        var br = $('<br/>').appendTo(div);
        var add = $('<input/>', {
            value : 'Добавить',
            type : 'button',
            'class' : 'AddDynamicExtraField'
        }).appendTo(div);
        add.click(function() {
            add.remove();
            addNewRowToList(div, j);
        });

    }

    //Для удаления первого поля
    $('.DeleteDynamicExtraField').click(function(event) {
        $(this).parent().remove();
        return false;
    });

</script>