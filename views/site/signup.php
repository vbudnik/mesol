<h1>Регистрация</h1>

<?php
    use  \yii\widgets\ActiveForm;
    use \yii\helpers\Html;
?>

<?php $form = ActiveForm::begin(['class'=>'form-horizontal']); ?>
<?= $form->field($model, 'email')->textInput(['autofocus'=>true]) ?>
<?= $form->field($model, 'password')->passwordInput() ?>
<?= Html::submitButton('submit', ['class'=> 'btn btn-primary']) ?>
<?php ActiveForm::end(); ?>