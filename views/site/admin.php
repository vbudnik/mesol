<h1>Блок - предоставление доступа менеджеру проекта администратором системы</h1>

<?php
    use \yii\widgets\ActiveForm;
    use \yii\helpers\Html;
?>

<p>id проекта -- надо заполнить</p>

<?php $form = ActiveForm::begin() ?>
<?= $form->field($project, 'nameproject')->textInput(['autofocus'=>true])->label("Короткое название проекта ") ?>
<?= $form->field($project, 'site')->textInput(['autofocus'=>true])->label("Сайт мероприятия") ?>
<?= $form->field($project, 'name')->textInput(['autofocus'=>true])->label("Менеджер проекта") ?>
<?= $form->field($project, 'email')->textInput(['autofocus'=>true])->label("Логин") ?>
<?= $form->field($project, 'password')->passwordInput()->label("Пароль") ?>

<?= Html::submitButton('Создать', ['class'=> 'btn btn-primary']) ?>

<?php $form = ActiveForm::end() ?>